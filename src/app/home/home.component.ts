import { Component, OnInit } from '@angular/core';
import { Quiz } from '../core/model/quiz.model';
import { IostService } from '../core/service/iost.service';
import { Identity } from '../core/model/identity.model';
import { QuizService } from '../core/service/quiz.service';
import { Router } from '@angular/router';
import * as bs58 from 'bs58';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayedColumns = ['quizName', 'sponsor', 'prizepool', 'startDateTime'];
  quizzes: Quiz[] = [];
  rem = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  };

  identity: Identity;
  encodedPublicKey: string;
  balance = {
    balance: 0
  };

  constructor(
    private iostService: IostService,
    private quizService: QuizService,
    private router: Router
  ) { }

  ngOnInit() {
    this.iostService.getOrCreateIdentity((identity) => {
      this.identity = identity;
      this.encodedPublicKey = bs58.encode(Buffer.from(this.identity.publicKey));
      this.iostService.getBalance((response) => this.balance = response);
    });

    this.quizService.getUpcomingQuizzes((quizzes) => {
      this.quizzes = quizzes;
      this.quizzes = this.quizzes.filter(q => q.startDateTime.getTime() - new Date().getTime() > 0);
      this.quizzes.sort((a, b) => a.startDateTime.getTime() - b.startDateTime.getTime());

      if (this.quizzes.length) {
        const that = this;
        const timer = setInterval(function () {
          let diff = Math.floor(that.quizzes[0].startDateTime.getTime() - new Date().getTime());
          if (diff <= 0) {
            that.rem.seconds = 0;
            that.rem.minutes = 0;
            that.rem.hours = 0;
            that.rem.days = 0;
            clearInterval(timer);
            that.quizService.selectedQuiz = that.quizzes[0];
            that.router.navigateByUrl('/quiz/' + that.quizzes[0].id);
            return;
          }

          diff = Math.floor(diff / 1000);
          that.rem.seconds = diff % 60;

          diff = Math.floor(diff / 60);
          that.rem.minutes = diff % 60;

          diff = Math.floor(diff / 60);
          that.rem.hours = diff % 24;

          diff = Math.floor(diff / 24);
          that.rem.days = diff;
        }, 1000);
      }
    });
  }
}

