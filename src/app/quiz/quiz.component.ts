import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Quiz } from '../core/model/quiz.model';
import { Question } from '../core/model/question.model';
import { Choice } from '../core/model/choice.model';
import { QuizService } from '../core/service/quiz.service';
import { IostService } from '../core/service/iost.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  quiz: Quiz;
  question: Question;
  selectedAnswer: Choice;
  correctAnswer: number;
  displayCorrectAnswer: boolean = false;
  done: boolean = false;
  correctCount: number = 0;
  remainingTime: number = 10;
  changeQuestionTime: number = 15;

  constructor(
    private route: ActivatedRoute,
    private quizService: QuizService,
    private iostService: IostService
  ) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.quiz = this.quizService.selectedQuiz;

    this.quizService.getQuestions(this.quiz.id, (questions) => {
      this.quiz.questions = questions;
      this.question = this.quiz.questions[0];
      this.resetRemainingTime();
    });
  }

  resetRemainingTime() {
    const that = this;
    const timer = setInterval(function () {
      if (that.remainingTime > 0) {
        if (that.remainingTime === 1) {
          that.quizService.getAnswer(that.quiz.id, that.question.questionNumber, (correctAnswer) => {
            if (that.selectedAnswer && correctAnswer === that.selectedAnswer.id) {
              that.correctCount++;
            }
            that.correctAnswer = correctAnswer;
          });
        }
        that.remainingTime--;
      } else {
        if (!that.selectedAnswer) {
          that.selectedAnswer = new Choice();
        }
        that.displayCorrectAnswer = true;
      }
      that.changeQuestionTime--;
      if (that.changeQuestionTime === 0) {
        clearInterval(timer);
        that.nextQuestion();
        if (!that.done) {
          that.remainingTime = 10;
          that.changeQuestionTime = 15;
          that.resetRemainingTime();
          that.correctAnswer = null;
          that.displayCorrectAnswer = false;
        }
      }
    }, 1000);
  }

  choose(choice: Choice) {
    this.selectedAnswer = choice;
    this.iostService.getOrCreateIdentity((identity) => {
      this.quizService.answerQuestion(this.quiz.id, this.question.questionNumber, choice.id, identity.accountName, () => {});
    });
  }

  nextQuestion() {
    if (this.question.questionNumber === this.quiz.questions.length) {
      this.done = true;
    } else {
      this.selectedAnswer = null;
      this.question = this.quiz.questions[this.question.questionNumber];
    }
  }
}