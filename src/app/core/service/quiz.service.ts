import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quiz } from '../model/quiz.model';
import { Question } from '../model/question.model';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  private baseUrl = 'http://192.168.50.232:8080';
  selectedQuiz: Quiz;

  constructor(private http: HttpClient) { }

  getUpcomingQuizzes(callback: (response: Quiz[]) => void) {
    this.http.get(this.baseUrl + '/api/quizzes')
      .pipe(
        map(response => {
          let quizzes = response as Quiz[];
          for (let quiz of quizzes) {
            quiz.startDateTime = new Date(quiz.startDateTime);
          }
          return quizzes;
        })
      )
      .subscribe((response) => callback(response as Quiz[]));
  }

  getQuestions(id: number, callback: (response: Question[]) => void) {
    this.http.get(this.baseUrl + '/api/quizzes/' + id + '/questions')
      .subscribe((response) => callback(response as Question[]));
  }

  answerQuestion(quizId: number, questionNumber: number, choiceId: number, accountName: string, callback: () => void) {
    this.http.post(this.baseUrl + '/api/quizzes/' + quizId + '/questions/' + questionNumber, {
      choiceId: choiceId, accountName: accountName
    }).subscribe(() => callback());
  }

  getAnswer(quizId: number, questionNumber: number, callback: (response: number) => void) {
    this.http.get(this.baseUrl + '/api/quizzes/' + quizId + '/questions/' + questionNumber)
      .subscribe((response) => callback(response as number));
  }
}
