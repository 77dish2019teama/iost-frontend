import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import * as IOST from 'iost';
import * as bs58 from 'bs58';
import { Identity } from '../model/identity.model';

const accountNameKey = 'ACCOUNT_NAME_KEY';
const identityKey = 'ID';
const publicKey = 'PUBLIC_KEY';

@Injectable({
  providedIn: 'root'
})
export class IostService {
  private _url = "http://192.168.50.232:30001";
  private _iost: IOST.IOST;
  private _rpc: IOST.RPC;
  private _creatorAccount: IOST.Account;

  constructor() {}

  getRpcInstance(): IOST.RPC {
    if (!this._rpc) {
      this._rpc = new IOST.RPC(new IOST.HTTPProvider(this._url));
    }
    return this._rpc;
  }

  getIostInstance(): IOST.IOST {
    if (!this._iost) {
      this._iost = new IOST.IOST({
        gasRatio: 1,
        gasLimit: 2000000,
        delay: 0,
      }, new IOST.HTTPProvider(this._url));
    }
    return this._iost;
  }

  getOrCreateIdentity(callback: (identity: Identity) => void): void {
    if (!localStorage.getItem(identityKey)) {
      const accountName = uuid().replace(/-/g, '').substring(0, 11);
      const kp = IOST.KeyPair.newKeyPair(IOST.Algorithm.Ed25519);
      console.log(kp);
      const newAccountTx = this.getIostInstance().newAccount(
        accountName,
        "admin",
        kp.id,
        kp.id,
        1024,
        1000
      );
        
      const account = this.getCreatorAccount();
      account.signTx(newAccountTx);
      const handler = new IOST.TxHandler(newAccountTx, this.getRpcInstance());
      handler
        .onSuccess(function (response) {
          console.log(response);
          localStorage.setItem(accountNameKey, accountName);
          localStorage.setItem(identityKey, bs58.encode(Buffer.from(kp.seckey)));
          localStorage.setItem(publicKey, bs58.encode(Buffer.from(kp.pubkey)));

          callback({
            id: bs58.decode(localStorage.getItem(identityKey)),
            publicKey: bs58.decode(localStorage.getItem(publicKey)),
            accountName: localStorage.getItem(accountNameKey)
          });
        })
        .send()
        .listen(1000, 5);
    } else {
      callback({
        id: atob(localStorage.getItem(identityKey)),
        publicKey: atob(localStorage.getItem(publicKey)),
        accountName: localStorage.getItem(accountNameKey)
      });
    }
  }

  getCreatorAccount(): IOST.Account {
    if (!this._creatorAccount) {
      this._creatorAccount = new IOST.Account("admin");
      const adminKp = new IOST.KeyPair(bs58.decode('2yquS3ySrGWPEKywCPzX4RTJugqRh7kJSo5aehsLYPEWkUxBWA39oMrZ7ZxuM4fgyXYs2cPwh5n8aNNpH5x2VyK1'));
      this._creatorAccount.addKeyPair(adminKp, "owner");
      this._creatorAccount.addKeyPair(adminKp, "active");
    }
    return this._creatorAccount;
  }

  getBalance(callback: (response: any) => void) {
    this.getOrCreateIdentity((identity) => {
      this.getRpcInstance().blockchain.getBalance(identity.accountName, 'iost')
        .then((response) => callback(response));
    });
  }
}
