import { Question } from './question.model';

export class Quiz {
    id: number;
    quizName: string;
    sponsor: string;
    prizepool: number;
    startDateTime: Date;
    questions?: Question[];
}
