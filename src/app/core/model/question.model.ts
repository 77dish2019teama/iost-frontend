import { Choice } from './choice.model';

export class Question {
    questionNumber: number;
    question: string;
    choices: Choice[];
}
