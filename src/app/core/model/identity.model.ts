export class Identity {
    id: string;
    publicKey: string;
    accountName: string;
}
