export class Account {
  constructor(id: any);
  addKeyPair(kp: any, permission: any): void;
  getID(): any;
  getKeyPair(permission: any): any;
  sign(t: any, permission: any): void;
  signTx(t: any): void;
}
export const Algorithm: {
  Ed25519: number;
  Secp256k1: number;
};
export namespace Bs58 {
  function decode(string: any): any;
  function decodeUnsafe(source: any): any;
  function encode(source: any): any;
}
export class HTTPProvider {
  constructor(host: any, timeout: any);
  send(method: any, url: any, data: any): any;
}
export class IOST {
  constructor(config: any);
  config: any;
  rpc: any;
  account: any;
  serverTimeDiff: any;
  callABI(contract: any, abi: any, args: any): any;
  currentAccount(): any;
  currentRPC(): any;
  newAccount(name: any, creator: any, ownerkey: any, activekey: any, initialRAM: any, initialGasPledge: any): any;
  setAccount(account: any): void;
  setRPC(rpc: any): void;
  signAndSend(tx: any): any;
  transfer(token: any, from: any, to: any, amount: any, memo: any): any;
}
export class KeyPair {
  static newKeyPair(algType: any): any;
  constructor(priKeyBytes: any, algType: any);
  t: any;
  seckey: any;
  pubkey: any;
  id: any;
  B58PubKey(): any;
  B58SecKey(): any;
}
export class RPC {
  constructor(provider: any);
  net: any;
  blockchain: any;
  transaction: any;
  getProvider(): any;
  setProvider(provider: any): void;
}
export class Signature {
  static fromJSON(json: any): any;
  constructor(info: any, keyPair: any);
  algorithm: any;
  pubkey: any;
  sig: any;
  toJSON(): any;
  verify(info: any): any;
}
export class Tx {
  constructor(gasRatio: any, gasLimit: any);
  gasRatio: any;
  gasLimit: any;
  actions: any;
  signers: any;
  signatures: any;
  publisher: any;
  publisher_sigs: any;
  amount_limit: any;
  chain_id: any;
  reserved: any;
  addAction(contract: any, abi: any, args: any): void;
  addApprove(token: any, amount: any): void;
  addPublishSign(publisher: any, kp: any): void;
  addSign(kp: any): void;
  addSigner(name: any, permission: any): void;
  setChainID(id: any): void;
  setGas(gasRatio: any, gasLimit: any): void;
  setTime(expirationInSecound: any, delay: any, serverTimeDiff: any): void;
}
export class TxHandler {
  static SimpleTx(contract: any, abi: any, args: any, config: any): any;
  constructor(tx: any, rpc: any);
  tx: any;
  Pending: any;
  Success: any;
  Failed: any;
  status: any;
  listen(interval: any, times: any): void;
  onFailed(c: any): any;
  onPending(c: any): any;
  onSuccess(c: any): any;
  send(): any;
}
